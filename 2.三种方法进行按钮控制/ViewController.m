//
//  ViewController.m
//  三种方法进行按钮控制
//  只有第一种frame封装方法中加入了动画效果，其余两种方法中未加入
//  Created by 廖家龙 on 2020/4/19.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//第一种方法基础版：frame的基本使用

//- (IBAction)up;//向上的方法
//- (IBAction)down;//向下的方法
//- (IBAction)left;//向左的方法
//- (IBAction)right;//向右的方法

//@property (weak, nonatomic) IBOutlet UIButton *image;//图片属性

//- (IBAction)big;//放大的方法
//- (IBAction)small;//缩小的方法

//第一种方法进阶版：frame的封装
@property (weak, nonatomic) IBOutlet UIButton *image;
- (IBAction)move:(UIButton *)sender;//移动方法
- (IBAction)smallandBig:(UIButton *)sender;//放大缩小


//第二种方法：center+bounds的封装使用
- (IBAction)centerMove:(UIButton *)sender;//移动方法
- (IBAction)boundsSandB:(UIButton *)sender;//放大缩小


//第三种方法：transform的封装使用
- (IBAction)transformMove:(UIButton *)sender;//移动方法
- (IBAction)transformSandB:(UIButton *)sender;//放大缩小
- (IBAction)transformXuanZhuan;//顺时针旋转方法
- (IBAction)transformXuanZhuan2;//逆时针旋转方法
- (IBAction)transformQingKong;//清空方法

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//第一种方法基础版：frame的基本使用

//- (IBAction)up {

//  思路：获取图片按钮的y坐标的值，让y的值递减，然后再把新的值赋值给图片按钮的y

//    1.获取图片按钮原始的frame值（这个值包含了按钮的位置和大小）
//    CGRect originFrame=self.image.frame;

//    2.设置y值-10
//    originFrame.origin.y -= 10;

//    3.把新的frame值赋值给图片按钮
//    self.image.frame=originFrame;

//注意：self.image.frame.origin.y -= 10；是不行的，当一个OC对象里面有一个属性，这个属性是一个结构体，你希望修改这个结构体里面的值的时候，不能直接修改
//}

//- (IBAction)down {
//    CGRect originFrame=self.image.frame;
//    originFrame.origin.y += 10;
//    self.image.frame=originFrame;
//}

//- (IBAction)left {
//    CGRect originFrame=self.image.frame;
//    originFrame.origin.x -= 10;
//    self.image.frame=originFrame;
//}

//- (IBAction)right {
//    CGRect originFrame=self.image.frame;
//    originFrame.origin.x += 10;
//    self.image.frame=originFrame;
//}

//- (IBAction)small {

//    CGRect originFrame=self.image.frame;

//    第一种写法：
//    originFrame.size.height -= 10;
//    originFrame.size.width -= 10;

//    第二种写法：
//    originFrame.size=CGSizeMake(originFrame.size.width - 10,originFrame.size.height - 10);

//    self.image.frame=originFrame;
//}

//- (IBAction)big {
//    CGRect originFrame=self.image.frame;
//    originFrame.size.height += 10;
//    originFrame.size.width += 10;
//    self.image.frame=originFrame;
//}

//第一种方法进阶版：frame的封装

//点击上下左右执行move方法
- (IBAction)move:(UIButton *)sender {
    
    //为每个按钮设置不同的tag值，然后这个方法中就可以根据sender.tag来判断用户当前点击的是哪个按钮
    
    //1.获取图片按钮原始的frame值（这个值包含了按钮的位置和大小）
    CGRect originFrame=self.image.frame;
    
    //2.修改frame
    switch (sender.tag) {
        case 10://上
            originFrame.origin.y -=10;
            break;
        case 20://下
            originFrame.origin.y +=10;
            break;
        case 30://左
            originFrame.origin.x -=10;
            break;
        case 40://右
            originFrame.origin.x +=10;
            break;
    }
    
//如果感觉位置移动和放大缩小的时候很生硬，可以增加一个动画效果
//第一种动画效果方式：头尾式
    //开启一个动画
    [UIView beginAnimations:nil context:nil];
    //设置动画执行时间
    [UIView setAnimationDuration:0.5];
    
    //3.把新的frame值赋值给图片按钮
    self.image.frame=originFrame;
    
    //提交动画
    [UIView commitAnimations];
}

//点击加减执行该方法
- (IBAction)smallandBig:(UIButton *)sender {
    
    //1.获取图片按钮原始的frame值（这个值包含了按钮的位置和大小）
    CGRect originFrame=self.image.frame;
    
    //2.修改frame
    switch (sender.tag) {
    case 200://放大
        originFrame.size.height +=10;
        originFrame.size.width +=10;
        break;
    case 300://缩小
        originFrame.size.height -=10;
        originFrame.size.width -=10;
        break;
    }
    
//第二种动画效果方式：Block式
    
    //3.把新的frame值赋值给图片按钮
    [UIView animateWithDuration:0.5 animations:^{
        self.image.frame=originFrame;
    }];
}

//第二种方法：center+bounds的封装使用

//先执行center方法修改图片的位置
- (IBAction)centerMove:(UIButton *)sender {
    //获取图片按钮原始的center值（这个值只包含了按钮的位置）
    CGPoint centerPoint=self.image.center;
    //修改center
    switch (sender.tag) {
        case 50://上
            centerPoint.y -=10;
            break;
        case 60://下
            centerPoint.y +=10;
            break;
        case 70://左
            centerPoint.x -=10;
            break;
        case 80://右
            centerPoint.x +=10;
            break;
    }
    //把新的center值赋值给图片按钮
    self.image.center=centerPoint;
}

//再执行bounds方法修改图片的大小
- (IBAction)boundsSandB:(UIButton *)sender {
    //获取图片按钮原始的bounds值（这个值只包含了按钮的大小）
    CGRect originBounds=self.image.bounds;
    //修改bounds的值
    if(sender.tag==400){
        originBounds.size.height+=10;
        originBounds.size.width+=10;
    }
    if(sender.tag==500){
        originBounds.size.height-=10;
        originBounds.size.width-=10;
    }
    //把新的bounds值赋值给图片按钮
    self.image.bounds=originBounds;
}

//第三种方法：transform的封装使用

//上下左右平移
- (IBAction)transformMove:(UIButton *)sender {
    
    //下面这句话的意思是告诉控件，平移到距离原始位置-10的距离
    //self.image.transform=CGAffineTransformMakeTranslation(0, -10);//向上平移，只能平移一次
    
    //基于一个现有的值进行平移
    switch (sender.tag) {
        case 90:
         //向上平移
         self.image.transform=CGAffineTransformTranslate(self.image.transform, 0, -10);
         break;
        case 100:
         //向下平移
         self.image.transform=CGAffineTransformTranslate(self.image.transform, 0, +10);
         break;
        case 110:
         //向左平移
         self.image.transform=CGAffineTransformTranslate(self.image.transform, -10, 0);
         break;
        case 120:
         //向右平移
         self.image.transform=CGAffineTransformTranslate(self.image.transform, +10, 0);
         break;
    }
}

//放大缩小
- (IBAction)transformSandB:(UIButton *)sender {
    
    //self.image.transform=CGAffineTransformMakeScale(0.5, 0.5);
    
    if(sender.tag==600){
        //放大
    self.image.transform=CGAffineTransformScale(self.image.transform, 1.5, 1.5);
    }
    if(sender.tag==700){
           //缩小
       self.image.transform=CGAffineTransformScale(self.image.transform, 0.5, 0.5);
       }
}

//顺时针旋转
- (IBAction)transformXuanZhuan {
    
    //顺时针旋转45度，可以一直旋转
    self.image.transform=CGAffineTransformRotate(self.image.transform, M_PI_4);
}

//逆时针旋转
- (IBAction)transformXuanZhuan2 {
    
   //逆时针旋转45度（这是旋转的弧度不是角度），可以一直旋转
   self.image.transform=CGAffineTransformRotate(self.image.transform, -M_PI_4);
    
    //逆时针旋转一次
    //self.image.transform=CGAffineTransformMakeRotation(-M_PI_4);
}

//一键清空
- (IBAction)transformQingKong {
    self.image.transform=CGAffineTransformIdentity;
}

@end
